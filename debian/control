Source: firefox-tokenserver
Section: net
Priority: optional
Maintainer: Phil Morrell <debian@emorrp1.name>
Build-Depends:
 debhelper (>= 11),
 dh-python,
 python-all,
 python-docutils,
 python-setuptools,
 python-sphinx,
Standards-Version: 4.1.4
Homepage: https://mozilla-services.readthedocs.io/en/latest/token/
Testsuite: autopkgtest-pkg-python
Vcs-Git: https://salsa.debian.org/emorrp1-guest/firefox-tokenserver.git
Vcs-Browser: https://salsa.debian.org/emorrp1-guest/firefox-tokenserver

Package: python-tokenserver
Architecture: all
Depends:
 ${misc:Depends},
 ${python:Depends},
Suggests: python-tokenserver-doc
Description: Firefox Sync token server
 This provides the login server part of a self-hosted Firefox Sync server.
 .
 This server defers authentication to the Mozilla-hosted accounts server
 at https://accounts.firefox.com.

Package: python-tokenserver-doc
Architecture: all
Section: doc
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Description: Firefox Sync token server (common documentation)
 This provides the login server part of a self-hosted Firefox Sync server.
 .
 This server defers authentication to the Mozilla-hosted accounts server
 at https://accounts.firefox.com.
 .
 This is the common documentation package.
